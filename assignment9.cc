#define ASSIGNMENT9_TEST
#ifdef  ASSIGNMENT9_TEST
#include <iostream>
#include <fstream>
#include <vector>
#include <list>
#include <sstream>
#include "assignment9.h"

using namespace std;

graph::graph(const char* filename)
{
    // cout << "File name: " << filename << endl;
    ifstream inputFile;
    inputFile.open(filename);

    // string parameters


    if (inputFile.is_open()) {
        string line;
        int lineNo = 0;
        while (std::getline(inputFile, line)) {
            // cout << "Read line: " << line << endl;

            // make sure the line has a contents
            if (line.size() > 0) {
                // get a string stream for this line
                istringstream iss(line);

                // first line contains the number of vertices
                if (lineNo == 0) {
                    iss >> graph::size;
                    // cout << "  Line 1: Setting size to " << graph::size << endl;
                }

                // second line contains the first row of the header table
                // this line could be ignored
                else if (lineNo == 1) {
                    // cout << "  Line 2: Nothing to do" << endl;
                    // nothing to do
                }

                // all following lines define the vertices and edges
                else {
                    char label;
                    int edgeExists;
                    list<int> edgeList;

                    iss >> label;
                    graph::label.push_back(label);
                    // cout << "  Line " << lineNo << ": Label: " << label << endl;

                    int vertexNo = 0;
                    while (iss >> edgeExists) {
                        edgeList.push_back(edgeExists);
                        // cout << "    Setting vertex " << vertexNo << " to " << edgeExists << endl;
                        vertexNo++;
                    }

                    // add the list to the
                    graph::adj_list.push_back(edgeList);

                    // create the vector and push it to adj_matrix
		    vector<int> edgeVector;
		    copy(edgeList.begin(), edgeList.end(), 
			 back_inserter(edgeVector));
                    // vector<int> edgeVector{ std::begin(edgeList), 
		    // std::end(edgeList)};
                    adj_matrix.push_back(edgeVector);
                }
            }

            lineNo++;
        }

        // initialize the isVisited vector
        for (int i=0; i<graph::size; i++)
            isVisited.push_back(false);

        // close the file
        inputFile.close();
    }
    else {
        cerr << "Could not open " << filename << endl;
    }

}


/**
 * The depth-first algorithm tries to go as far down a path as possible before it tries an alternate path.
 */
void graph::depth_first(int v) {

    // set the visted node to true
    isVisited[v] = true;

    // check for any adjacent vertices
    for (int col = 0; col < size; col++) {

        // found an adjacent node and that node is not visited
        if (adj_matrix[v][col] != 0 && !isVisited[col]) {
            // capture the path
            ostringstream oss;
            oss << "(" << label[v] << ", " << label[col] << ")";
            path.push_back(oss.str());

            // traverse into that node
            cout << label[col] << " ";
            depth_first(col);
        }
    }
}



/*
 * Loop over all vertices and traverse into any unvisited vertices.
 */
void graph::traverse()
{

    for (int i=0; i<size; i++) {
        if (!isVisited[i]) {
            cout << label[i] << " ";
            depth_first(i);
        }
    }
    cout << endl;

    // print out the path
    for (vector<string>::iterator it = path.begin(); it != path.end(); ++it)
        cout << *it << " ";
    cout << endl;
}

/*
 * Print adjacency list for the graph
 * 1. Iterate over all vertices i = 0..(size-1)
 * 1.1 Start printing the new line like label[i]:
 * 1.2 Loop over all vertices j = 0..(size-1)
 * 1.1.1 If the value of edge[i][j] is 1 then print label[j]
 * 1.3 print new line
 */
void graph::print() const
{
    //
    cout << "Number of vertices in the graph: " << graph::size << endl;
    cout << endl;
    cout << endl;


    // loop over all vertices
    cout << "-------- graph -------" << endl;
    int i;
    for (i=0; i < graph::size; i++) {
        cout << label[i] << ": ";

        // iterate over all edges
        list<int> edgeList = adj_list.at(i);

        int vertexNo = 0;
        list<int>::iterator it;
        for (it = edgeList.begin(); it != edgeList.end(); ++it) {
            if (*it > 0) cout << graph::label[vertexNo] << ", ";
            vertexNo++;
        }
        cout << endl;
    }
    cout << "------- end of graph ------" << endl;
}

graph::~graph() {

}



int main(int argc, char** argv)
{
	if ( argc < 2 )
	{
		cerr << "args: input-file-name\n";
		return 1;
	}
	graph g(argv[1]);
	g.print();
	g.traverse();
	return 0;
}

#endif
