//
// Created by Will Roethel on 12/3/15.
//

#define CATCH_CONFIG_MAIN

#include "../assignment9.h"
#include "catch.hpp"

using namespace std;

TEST_CASE("Constructor reading input") {
    graph g("./assignment9input.txt");

    g.print();
}
