#ifndef ASSIGNMENT9_H
#define ASSIGNMENT9_H

#include <vector>
#include <list>

class graph {
private:
    int size;

    // WR Correction: Variable is called adj_matrix in assignemnt9.cc
    std::vector<std::list<int> > adj_list;

    std::vector< std::vector<int> > adj_matrix;

    // WR Correction. Variable is referred to as label in assignment9.cc
    // std::vector< char > labels;
    std::vector<char> label;

    // track the visited vertices
    std::vector<bool> isVisited;

    std::vector<std::string> path;

    void depth_first(int);

public:
    graph(const char *filename);

    ~graph();

    int get_size() const;

    void traverse();

    void print() const;

};

#endif
